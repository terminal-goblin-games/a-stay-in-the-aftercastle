extends KinematicBody2D

var velocity = Vector2.ZERO
var speed = 500
var acceleration = 0.5
var friction = 0.3

enum states {IDLE, WALKING, DEAD, HURT}
enum act_states {IDLE,ATTACKING, ATTACKING_UP, ATTACKING_DOWN}
var state = states.IDLE
var act_state = act_states.IDLE

var score = 0
var max_hp = 100
var current_hp = 100
var dmg = 25
var can_be_hit = true
var can_input = true
var can_pause = false
var is_paused = false
var main = Main

var shake_amt = 15
var is_shake = false

# Handles our input
func get_input():
	if can_input && !is_paused:
		var dir = Vector2.ZERO
		if Input.is_action_pressed("up"):
			dir = Vector2(0,-1)
		if Input.is_action_pressed("down"):
			dir = Vector2(0,1)
		if Input.is_action_pressed("left"):
			dir = Vector2(-1,0)
			set_sprites_and_hb("LEFT")
		if Input.is_action_pressed("right"):
			dir = Vector2(1,0)
			set_sprites_and_hb("RIGHT")
		if Input.is_action_pressed("right") && Input.is_action_pressed("up"):
			dir = Vector2(1,-1)
		if Input.is_action_pressed("right") && Input.is_action_pressed("down"):
			dir = Vector2(1,1)
		if Input.is_action_pressed("left") && Input.is_action_pressed("down"):
			dir = Vector2(-1,1)
		if Input.is_action_pressed("left") && Input.is_action_pressed("up"):
			dir = Vector2(-1,-1)
		if dir != Vector2.ZERO:
			state = states.WALKING
			velocity = lerp(velocity, dir * speed, acceleration)
		else:
			state = states.IDLE
			velocity = Vector2.ZERO
	if can_pause:
		if Input.is_action_just_pressed("pause"):
			if is_paused:
				is_paused = false
				get_tree().paused = false
				$CanvasLayer/PauseScreen.visible = false
			else:
				is_paused = true
				get_tree().paused = true
				$CanvasLayer/PauseScreen.visible = true


# Called when the node enters the scene tree for the first time.
func _ready():
	$CanvasLayer/PauseScreen.visible = false
	if main.first_night == true:
		$CanvasLayer/NightStartScreen/.visible = true
		can_input = false
		can_pause = false
		get_tree().paused = true
	else:
		$CanvasLayer/NightStartScreen.visible = false
		$CanvasLayer/GameTime.start()
		can_input = true
		can_pause = true
		get_tree().paused = false
		Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	main.set_first_night()
	$CanvasLayer/VBoxContainer/Hp_Bar.value = current_hp
	$"CanvasLayer/ GameOverScreen".visible = false
	$CanvasLayer/NightFinishScreen.visible = false
	#$CanvasLayer/GameTime.start()


# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	state_machine()
	get_input()
	move_and_slide(velocity)
	update_score()
	update_timer()
	if is_shake:
		shake_cam()
	else:
		set_cam_back()
	sword_control()

# Handles our state, used for animation, etc
func state_machine():
	match state:
		states.IDLE:
			match act_state:
				act_states.IDLE:
					$AnimationPlayer.play("Idle")
				act_states.ATTACKING:
					$AnimationPlayer.play("Attacking")
					#attack()
					#swing_sword()
					pass
				act_states.ATTACKING_UP:
					$AnimationPlayer.play("Attacking_Up")
				act_states.ATTACKING_DOWN:
					$AnimationPlayer.play("Attacking_Down")
		states.WALKING:
			match act_state:
				act_states.IDLE:
					$AnimationPlayer.play("Walking")
				act_states.ATTACKING:
					$AnimationPlayer.play("Attacking")
				act_states.ATTACKING_UP:
					$AnimationPlayer.play("Attacking_Up")
				act_states.ATTACKING_DOWN:
					$AnimationPlayer.play("Attacking_Down")
				act_states.ATTACKING_UP:
					$AnimationPlayer.play("Attacking_Up")
				act_states.ATTACKING_DOWN:
					$AnimationPlayer.play("Attacking_Down")
		states.HURT:
			can_be_hit = false
			$AnimationPlayer.play("Hurt")
			is_shake = true
			can_input = false
			yield(get_tree().create_timer(0.05),"timeout")
			is_shake = false
			
		states.DEAD:
			$AnimationPlayer.play("Dead")
			can_input = false
			$CanvasLayer/GameTime.stop()
			$"CanvasLayer/ GameOverScreen".visible = true
			$"CanvasLayer/ GameOverScreen/Score".text = String(main.ovr_score)
			Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)

func _on_AnimationPlayer_animation_finished(anim_name):
	if state != states.DEAD:
		state = states.IDLE
		act_state = act_states.IDLE
		can_be_hit = true
	yield(get_tree().create_timer(0.3),"timeout")
	can_input = true

func _on_Area2D_body_entered(body):
	is_shake = true
	yield(get_tree().create_timer(0.05),"timeout")
	is_shake = false
	
func score(num):
	score += num

func update_score():
	$CanvasLayer/VBoxContainer2/TextureRect/Score.text = String(score)

func update_timer():
	$CanvasLayer/VBoxContainer2/TextureRect/Timer.text = String(int($CanvasLayer/GameTime.time_left))

func hit(dmg, name):
	if can_be_hit:
		state = states.HURT
		knockback(name)
		current_hp -= dmg
		if current_hp <= 0:
			state = states.DEAD
		$CanvasLayer/VBoxContainer/Hp_Bar.value = current_hp

func set_sprites_and_hb(dir):
	if dir == "RIGHT":
		$Sprite.scale.x = 1
	if dir == "LEFT":
		$Sprite.scale.x = -1

func knockback(name):
	var pos = get_parent().get_node(name).global_position
	velocity = (global_position - pos).normalized() * speed
	#move_and_slide(velocity)



func _on_GameTime_timeout():
	main.overall_score(score)
	$CanvasLayer/NightFinishScreen/TextureRect/NightScore.text = "Night score: " + String(score)
	$CanvasLayer/NightFinishScreen/TextureRect/OverScore.text = "Overall score: " + String(main.ovr_score)
	$CanvasLayer/NightFinishScreen.visible = true
	get_tree().paused = true
	Input.set_mouse_mode(Input.MOUSE_MODE_VISIBLE)
	$CanvasLayer/GameTime.stop()
	


func shake_cam():
	$Camera2D.set_offset(Vector2(rand_range(-1.0, 1.0) * shake_amt, rand_range(-1.0, 1.0) * shake_amt))
func set_cam_back():
	$Camera2D.set_offset(Vector2.ZERO)

func sword_control():
	$Rot_pos.rotation = get_angle_to(get_global_mouse_position())
	$Rot_pos/Sword_Pos.look_at(get_global_mouse_position())# = get_angle_to(get_global_mouse_position())

# Adds to our hp, updates our bar 
# The clamp is to make sure we don't go over 100
func add_hp(hp):
	if current_hp != 100:
		current_hp += hp
		clamp(current_hp, 1, 100) 
	$CanvasLayer/VBoxContainer/Hp_Bar.value = current_hp

# Button that closes the info popup at the beginning of each night
func _on_TextureButton_pressed():
	$CanvasLayer/NightStartScreen.visible = false
	$CanvasLayer/GameTime.start()
	can_input = true
	can_pause = true
	get_tree().paused = false
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)
	


func _on_NextNightButton_button_down():
	get_tree().change_scene("res://Scenes/World.tscn")


func _on_TextureButton_button_down():
	get_tree().change_scene("res://Scenes/World.tscn")
