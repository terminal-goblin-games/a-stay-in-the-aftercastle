extends Node2D

# Vars for the map
var max_x = 100
var max_y = 100
var max_rooms = 15

# Preloads for generation
var player = preload("res://Scenes/Player.tscn")
var barrel = preload("res://Scenes/Barrel.tscn")
var ghost = preload("res://Scenes/Ghost.tscn")
var butler = preload("res://Scenes/Butler.tscn")

# Things to keep track of
var spawn_x
var spawn_y
enum tiles {WALL, ROCK, FLOOR}
var datamap = []
var start_pos = Vector2()
var pds = []
var floors = []

# Vars for our world inhabitants
var max_barrels
var cur_barrels = 0
var max_ghosts
var cur_ghosts = 0
var max_butlers
var cur_butlers = 0


# Called when the node enters the scene tree for the first time.
func _ready():
	generate()

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	pass

# Main generation function
# Calls a randi()%int+min that will determine how many of a specific thing will spawn. Max things = int+min
func generate():
	randomize()
	max_barrels = randi()%50+15.0
	randomize()
	max_ghosts = randi()%30+10
	randomize()
	max_butlers = randi()%15+5
	fill_with_rock()
	create_rooms()
	player_spawn(spawn_x, spawn_y+1)
	
	# Generates our doors pds = Potential Doors 
	# pds is a storage of all Wall tiles 
	var i = 0
	var closest = 0
	for pd in pds:
		if i < (pds.size()-1):
			make_corridors(pd, pds[i+1])
		else:
			break
		i += 1
	place_barrels()
	place_ghosts()
	place_butlers()


# Step through all of the tiles and fill it with rock
func fill_with_rock():
	var y = 0
	var x = 0
	while y != max_y:
		while x != max_x:
			set_tile(x,y, tiles.ROCK)
			x += 1
		y += 1
		x = 0

func create_rooms():
	randomize()
	var test_x = 15
	var test_y = 15
	var origin_y = randi()%(max_y-20)+10
	var origin_x = randi()%(max_x-20)+10
	spawn_x = origin_x
	spawn_y = origin_y
	var rooms = 0
	while rooms != max_rooms:
		set_tile(origin_x,origin_y, tiles.FLOOR)
		var i = 0
		var j = 0
		while i != 5:
			while j != 5:
				set_tile(origin_x + j, origin_y + i, tiles.FLOOR)
				j += 1
				floors.append(Vector2(origin_x+j, origin_y + i))
			set_tile(origin_x + j, origin_y + i, tiles.WALL)
			set_tile(origin_x - 1, origin_y + i, tiles.WALL)
			i += 1
			j = 0
		pds.append(Vector2(origin_x,origin_y))
		origin_y = randi()%(max_x-20)+10
		origin_x = randi()%(max_x-20)+10
		rooms += 1


func set_tile(x,y, type):
	$TileMap.set_cell(x,y,type)

func player_spawn(x,y):
	var play = player.instance()
	play.position = Vector2(x * 48, y * 48)
	add_child(play)

# Makes corridors between our rooms
func make_corridors(point1, point2):
	var dirx = point1.x - point2.x
	var diry = point1.y - point2.y
	var i
	var j
	
	if randf() > 0.5:
		i = 0
		j = -1 if (dirx > 0) else 1
		while abs(i) != abs(dirx):
			set_tile(point1.x+i, point1.y, tiles.FLOOR)
			floors.append(Vector2(point1.x+i, point1.y))
			i += j
		
		i = 0
		j = -1 if (diry > 0) else 1
		while abs(i) != abs(diry):
			set_tile(point1.x, point1.y+i, tiles.FLOOR)
			floors.append(Vector2(point1.x, point1.y+i))
			i += j
	
	else:
		i = 0
		j = -1 if (diry > 0) else 1
		while abs(i) != abs(diry):
			set_tile(point1.x, point1.y+i, tiles.FLOOR)
			floors.append(Vector2(point1.x, point1.y+i))
			i += j
		
		i = 0
		j = -1 if (dirx > 0) else 1
		while abs(i) != abs(dirx):
			set_tile(point1.x+i, point1.y, tiles.FLOOR)
			floors.append(Vector2(point1.x+i, point1.y))
			i += j

func place_barrels():
	var coords = randi()%floors.size()
	while cur_barrels != max_barrels:
		var bar = barrel.instance()
		bar.position = Vector2(floors[coords].x * 48, floors[coords+1].y * 48) # This is multiplied by 48 because it is the cell size
		add_child(bar)
		cur_barrels += 1
		floors.remove(coords) # Removing from our arrays so we don't double stack spawns
		randomize()
		coords = randi()%floors.size()

func place_ghosts():
	var coords = randi()%floors.size()
	while cur_ghosts != max_ghosts:
		var gho = ghost.instance()
		gho.position = Vector2(floors[coords].x * 48, floors[coords].y * 48) # This is multiplied by 48 because it is the cell size
		add_child(gho)
		floors.remove(coords) # Removing from our arrays so we don't double stack spawns
		cur_ghosts += 1
		randomize()
		coords = randi()%floors.size()

func place_butlers():
	var coords = randi()%floors.size()
	if floors[coords].y == floors.size():
		coords = floors[coords-2]
	while cur_butlers != max_butlers:
		var but = butler.instance()
		but.position = Vector2(floors[coords].x * 48, floors[coords+1].y * 48) # This is multiplied by 48 because it is the cell size
		add_child(but)
		floors.remove(coords) # Removing from our arrays so we don't double stack spawns
		cur_butlers += 1
		randomize()
		coords = randi()%floors.size()
