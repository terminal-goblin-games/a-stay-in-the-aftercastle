extends Node2D

var ovr_score = 0
var first_night = true
# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Button_button_down():
#	randomize()
#	var level = randi()%3+1
#	match level:
#		1:
#			get_tree().change_scene("res://Scenes/Level1.tscn")
#		2:
#			get_tree().change_scene("res://Scenes/Level2.tscn")
#		3:
#			get_tree().change_scene("res://Scenes/Level3.tscn")
	get_tree().change_scene("res://Scenes/World.tscn")

func overall_score(score):
	ovr_score += score
	print(ovr_score)

func set_first_night():
	first_night = false

func _on_TextureButton_button_down():
#	randomize()
#	var level = randi()%3+1
#	match level:
#		1:
#			get_tree().change_scene("res://Scenes/Level1.tscn")
#		2:
#			get_tree().change_scene("res://Scenes/Level2.tscn")
#		3:
#			get_tree().change_scene("res://Scenes/Level3.tscn")
	get_tree().change_scene("res://Scenes/World.tscn")


func _on_TextureButton2_button_down():
	$CanvasLayer/VBoxContainer/TextureButton2/TextureRect2.visible = true


func _on_TextureButton3_button_down():
	$CanvasLayer/TextureButton3.visible = false


func _on_TextureButton_pressed():
	$CanvasLayer/VBoxContainer/TextureButton2/TextureRect2.visible = false


func _on_deb_button_down():
	get_tree().change_scene("res://Scenes/Debug_lvl.tscn")
