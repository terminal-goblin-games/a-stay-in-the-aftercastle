extends KinematicBody2D

var speed = 200
var current_hp = 100
var plate = preload("res://Scenes/Plate.tscn")
var velocity = Vector2.ZERO
enum states {IDLE, WALKING, THROWING, HURT, DEAD}
var state = states.IDLE
var can_throw = true
var rate_of_fire = 1
var move_away = false
var move_away_min = 0.5
var target
var can_move = true
var can_score = true
var player
# Called when the node enters the scene tree for the first time.
func _ready():
	add_to_group("enemies")
	get_parent().get_node("Player")

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta):
	#chase_player(velocity)
	check_distance()
	ai_get_dir()
	if target != null:
		ai_move()
		throw_plate()
	state_machine()

# Old function to chase the player by subtracting the players position from the butlers
# Also included a first take on moving away
# Keeping for now incase I choose to abandon the new system of ai_get_dir() + ai_move()
func chase_player(velocity):
	if player != null && !move_away: 
		state = states.WALKING
		velocity = (player.global_position - global_position).normalized() * speed
	elif player != null && move_away:
		state = states.WALKING
		if(global_position < (player.global_position + Vector2(500,500))):
			velocity = (player.global_position + global_position).normalized() * speed
			move_and_slide(velocity)
	move_and_slide(velocity)

func throw_plate():
	if target != null:
		if can_throw:
			can_throw = false
			var pl = plate.instance()
			pl.global_position = $Plate_pos.global_position
			get_parent().add_child(pl)
			pl.rotation = get_angle_to(target.global_position)
			pl.shoot(target.global_position)
			yield(get_tree().create_timer(rate_of_fire), "timeout")
			can_throw = true

func hit(dmg):
	state = states.HURT
	current_hp -= dmg
	print(current_hp)
	can_move = false
	can_throw = false
	if current_hp <= 0:
		state = states.DEAD

func _on_Area2D_body_entered(body):
	if body.get_name() == "Player":
		target = body

func state_machine():
	match state:
		states.IDLE:
			pass
		states.WALKING:
			$AnimationPlayer.play("walking")
		states.THROWING:
			$AnimationPlayer.play("throw")
		states.DEAD:
			can_throw = false
			can_move = false
			$AnimationPlayer.play("dead")
		states.HURT:
			can_move = false
			$AnimationPlayer.play("hurt")
			can_throw = false


func _on_Area2D_body_exited(body):
#	if body.get_name() == "Player":
#		player = body
	pass

func ai_get_dir():
	if target != null && can_move:
		if move_away:
			return target.position + self.position
		return target.position - self.position

func ai_move():
	if can_move:
		state = states.WALKING
		var dir = ai_get_dir()
		var motion = dir.normalized() * speed
		state = states.WALKING
		move_and_slide(motion)

func check_distance():
	if target != null && can_move:
		var check = int(position.distance_to(target.position))
		if check < 100:
			move_away = true
		else:
			if move_away:
				yield(get_tree().create_timer(move_away_min), "timeout")
				move_away = false
			else:
				move_away = false


func _on_AnimationPlayer_animation_finished(anim_name):
	if state != states.DEAD:
		state = states.IDLE
		can_move = true
		can_throw = true
	else:
		if can_score: 
			can_score = false
			can_throw = false
			can_move = false
			target.score(30)
			$Sprite.visible = false
			$Popup.visible = true
			$CollisionShape2D.disabled = true
			$Platehb.disabled = true
			yield(get_tree().create_timer(1), "timeout")
			queue_free()
		else:
			can_throw = false
			can_move = false
			$Sprite.visible = false
			$Popup.visible = true
			$CollisionShape2D.disabled = true
			$Platehb.disabled = true
			yield(get_tree().create_timer(1), "timeout")
			queue_free()
			
