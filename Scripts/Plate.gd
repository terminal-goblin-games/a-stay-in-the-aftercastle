extends RigidBody2D

var speed = 600

# Called when the node enters the scene tree for the first time.
func _ready():
	pass


# Called every frame. 'delta' is the elapsed time since the previous frame.
#func _process(delta):
#	pass


func _on_Plate_body_entered(body):
	if body.get_name() == "Player":
		body.hit(10, self.get_name())
	else:
		queue_free()

func shoot(pos):
	apply_impulse(Vector2(), Vector2(speed, 0).rotated(rotation))
