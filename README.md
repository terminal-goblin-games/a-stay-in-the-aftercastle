# A Stay In The Aftercastle
An arcade game based around surviving nights in a haunted casttle. Get all the loot and kill all the ghosts you can before daylight!

# Mechanics
The closer the timer gets to 0 the less health the ghosts have, but the more damage they do! Smash all the barrels and collect all the goblets you can before the timer runs out

You must survive the entire night for your score to be added to your overall score

# Controls
WASD to move
Swing mouse around to hit things with your sword

# About
This was made as a personal game jam project to create a fully realized game in a weekends time
It is now expanded to have procedural dungeons

# Installation
You can clone this repo and open it in Godot
Or use one of the provided binaries
A third option is downloading from Itch.io but note this one is usually a few versions behind: https://terminalgoblin.itch.io/a-stay-in-the-aftercastle

# Donations
This game will always be free, but if you really really like it I now accept crypto donations. If you don't want to send me magic internet money please email me and we will work something out john@terminalgoblingames.com

Bitcoin: bc1qw pyjt 0dp5 4aj6 xdr7 ssmx kd8v als9 6f3p myd0s

Ethereum: 0xBD590173A073b0FB228fDB6a91Bf7Da7B128ED9C

Ripple: rH3JmMigZAD5ydZTNvUrxLSGGjheGPsWcv

Dogecoin: D9H1ZPabVqYhFYkGgw4bndVCFv6Nj81wyc